# gitlab-ci-shellspec

A collection of helper functions for unit testing `script` clauses of GitLab CI/CD configuration files (*.gitlab-ci.yml*) via [ShellSpec](https://github.com/shellspec/shellspec).

## Overview

Assume the following *.gitlab-ci.yml* pipeline configuration: 

```yaml
variables:
  USER: Michael

job-1:
  script:
    - echo "Hello, World!"
    - echo "Hello, $USER!"
```

A unit test can verify the output:

```bash
#shellcheck shell=bash

Describe '.gitlab-ci.yml'
  BeforeEach ci_spec::export_all_global_variables

  It 'says hello to the world and to me'
    When call ci_spec::execute_job  'job-1'
    The lines of output should equal 2
    The 1st line should equal 'Hello, World!'
    The 2nd line should equal 'Hello, Michael!'
  End
End
```

The topic is also discussed in the blog post on [Testing GitLab CI/CD job scripts](https://mgrafl.wordpress.com/2023/12/18/testing-gitlab-ci-cd-job-scripts/).

## Usage

The easiest way to run the unit tests is via the dedicated Docker image [mgrafl/gitlab-ci-shellspec](https://hub.docker.com/r/mgrafl/gitlab-ci-shellspec). This is the preferred usage for local development and GitLab CI/CD integration.

### Docker

To execute unit tests locally via Docker, run:

```
docker run --rm -t -v ".:/src" mgrafl/gitlab-ci-shellspec
```

#### Tests using Docker

The Docker image also has a variant that has Docker installed in the container. 
This enables tests that ramp up another Docker container. 
In order to give the container access to the Docker host by mounting the Docker socket.

Under Linux:
```
docker run --rm -t -v ".:/src" -v "/var/run/docker.sock:/var/run/docker.sock" mgrafl/gitlab-ci-shellspec:docker
```

Under Windows (with an additional slash at the beginning of the mount source):
```
docker run --rm -t -v ".:/src" -v "//var/run/docker.sock:/var/run/docker.sock" mgrafl/gitlab-ci-shellspec:docker
```


### GitLab `.gitlab-ci.yml`

In the GitLab CI/CD configuration file `.gitlab-ci.yml`, use the Docker image, but override the entrypoint (see GitLab documentation on  [overriding the entrypoint of an image](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#override-the-entrypoint-of-an-image)).

```yaml
shellspec:
  image: 
    name: mgrafl/gitlab-ci-shellspec
    entrypoint: [""]
  stage: test
  script: 
    - gitlab-ci-shellspec
```

<details>
  <summary>
    <h3>Linux</h3>
  </summary>

Prefer the dedicated Docker image over local installation. 
Local installation instructions are only provided for the sake of completeness.

The dependency [yq](https://github.com/mikefarah/yq/) must be installed.

Assuming the code from this repository is located in `/path/to/gitlab-ci-shellspec/`, run `shellspec` directly as:

```sh
shellspec --shell=/bin/bash --load-path=/path/to/gitlab-ci-shellspec --require ci_spec_helper
```

#### With ShellSpec Extension: Invocation

Unit tests can also use the ShellSpec extension for mock invocations from the GitHub project [mgrafl/shellspec-ext-invocation](https://github.com/mgrafl/shellspec-ext-invocation).
Assuming the code from `mgrafl/shellspec-ext-invocation` is located in `/path/to/shellspec-ext-invocation/`, run `shellspec` directly as:

```sh
shellspec --shell=/bin/bash --load-path=/path/to/shellpec-ext-invocation/lib/extension/invocation --require capture_invocation_helper --load-path=/path/to/gitlab-ci-shellspec --require ci_spec_helper
```

or:


```sh
PATH_TO_SHELLSPEC_EXT_INVOCATION="/path/to/shellspec-ext-invocation/"
PATH="${PATH_TO_SHELLSPEC_EXT_INVOCATION}:${PATH}"
chmod +x "${PATH_TO_SHELLSPEC_EXT_INVOCATION}shellspec-ext-invocation"


PATH_TO_GITLAB_CI_SHELLSPEC="/path/to/gitlab-ci-shellspec/"
PATH="${PATH_TO_GITLAB_CI_SHELLSPEC}:${PATH}"
chmod +x "${PATH_TO_GITLAB_CI_SHELLSPEC}gitlab-ci-shellspec"

# Append ShellSpec CLI parameters as needed
gitlab-ci-shellspec [shellspec-cli-params ...]
```

[ShellSpec CLI](https://github.com/shellspec/shellspec#shellspec-cli) parameters can be appended to the command.
</details>

## Features

NOTE:
This project focuses on unit testing the `script` clauses alone as opposed to integration testing the entire GitLab CI/CD pipeline.

The helper functions are located in *lib/extension/ci_spec*. They are implemented in [Bash](https://www.gnu.org/software/bash/) and prefixed with the package name `ci_spec::`.

The main helper functions are: 

<dl>
  <dt><code>ci_spec::export_all_global_variables</code></dt>
  <dd>Exports all entries from the global variables section of the GitLab CI/CD configuration file, expanding any environment variables.</dd>

 <dt><code>ci_spec::export_all_job_variables</code></dt>
 <dd>Exports all entries from the given job's variables section of the GitLab CI/CD configuration file, expanding any environment variables.</dd>

 <dt><code>ci_spec::export_global_variable</code></dt>
 <dd>Exports the variable value from the global variables section of the GitLab CI/CD configuration file, expanding any environment variables.</dd>

 <dt><code>ci_spec::export_job_variable</code></dt>
 <dd>Exports the variable value from a job section of the GitLab CI/CD configuration file, expanding any environment variables.</dd>

 <dt><code>ci_spec::export_variable</code></dt>
 <dd>Exports a single variable's value from the GitLab CI/CD configuration file, expanding any environment variables.</dd>

 <dt><code>ci_spec::execute_job</code></dt>
 <dd>Executes the script clause of the referenced job of the GitLab CI/CD configuration file.</dd>

 <dt><code>ci_spec::execute_scriptlet</code></dt>
 <dd>Executes the scriptlet denoted by the path elements from the GitLab CI/CD configuration file. A scriptlet consists of one or more entries of a script clause (or of a before_script or after_script clause).</dd>
</dl>

For details about function arguments, refer directly to the shell script: [lib/extension/ci_spec/ci_spec_helper.sh](lib/extension/ci_spec/ci_spec_helper.sh)

The functions use [yq](https://github.com/mikefarah/yq/) to extract `script` and `variable` clauses. 

Variables are expanded based on [GitLab's internal variable expansion mechanism](https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html#gitlab-internal-variable-expansion-mechanism). That is, the expanded part needs to be in a form of `$variable`, or `${variable}` or `%variable%`.

The Docker image extends from [mgrafl/shellspec-ext-invocation](https://hub.docker.com/r/mgrafl/shellspec-ext-invocation). The ShellSpec extension enables bash-based ShellSpec tests to capture mock invocations and to verify expectations about these invocations.


## Other resources

* [ShellSpec](https://github.com/shellspec/shellspec)
* Docker image: [mgrafl/gitlab-ci-shellspec](https://hub.docker.com/r/mgrafl/gitlab-ci-shellspec)
* Blog posts: 
  * **[Docker-based ShellSpec Tests for GitLab CI/CD Job Scripts](https://mgrafl.wordpress.com/2024/07/06/docker-based-shellspec-tests-for-gitlab-ci-cd-job-scripts/)**
  * [Testing GitLab CI/CD job scripts](https://mgrafl.wordpress.com/2023/12/18/testing-gitlab-ci-cd-job-scripts/)
* ShellSpec Extension for capturing mock invocations: https://github.com/mgrafl/shellspec-ext-invocation


## Contributing

Contributions to this project are always welcome. 
Please read the [contributing section](CONTRIBUTING.md) and raise a Merge Request.

## License
This project is licensed under the [MIT license](LICENSE).
