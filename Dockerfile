# Set the BASE_IMAGE_TAG_SUFFIX to "-docker" in order to build an image that has also docker installed.
ARG BASE_IMAGE_TAG_SUFFIX=

FROM mgrafl/shellspec-ext-invocation:1.1.0${BASE_IMAGE_TAG_SUFFIX}

LABEL org.opencontainers.image.authors="Michael Grafl (https://gitlab.com/mgrafl)" \
      org.opencontainers.image.ref.name="mgrafl/gitlab-ci-shellspec"

# Based on https://github.com/mikefarah/yq/blob/v4.40.5/Dockerfile
COPY --from=mikefarah/yq /usr/bin/yq /usr/bin/yq

ENV PATH_TO_GITLAB_CI_SHELLSPEC /opt/gitlab-ci-shellspec/
ENV PATH "${PATH_TO_GITLAB_CI_SHELLSPEC}:${PATH}"
COPY lib/extension/ci_spec LICENSE ${PATH_TO_GITLAB_CI_SHELLSPEC}lib/extension/ci_spec/
COPY gitlab-ci-shellspec ${PATH_TO_GITLAB_CI_SHELLSPEC}
RUN chmod +x "${PATH_TO_GITLAB_CI_SHELLSPEC}gitlab-ci-shellspec"

ENTRYPOINT [ "gitlab-ci-shellspec" ]
