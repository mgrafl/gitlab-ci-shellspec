#shellcheck shell=bash
#
# Helper functions for GitLab CI/CD unit testing via shellspec.


#######################################
# Exports all entries from the global variables section of the GitLab CI/CD configuration file, expanding any environment variables.
# Entries that have a "description" are omitted because they must be set via UI.
# Globals:
#   CI_CONFIG_PATH - the GitLab CI/CD configuration file (defaults to ".gitlab-ci.yml")
# Arguments:
#   (none)
#######################################
ci_spec::export_all_global_variables() {
  local __yaml_global_variables
  # Get the variables map, 
  # then continue with the entries where the value do not have a "description" key, 
  # then use their keys, and
  # finally splat the array
  __yaml_global_variables=$(yq eval '.variables | with_entries( select( .value | has("description") | not ) ) | keys | .[]' -- "${CI_CONFIG_PATH:-.gitlab-ci.yml}")
  
  for __var in $__yaml_global_variables; do
    ci_spec::export_global_variable "$__var"
  done
}

#######################################
# Exports all entries from the given job's variables section of the GitLab CI/CD configuration file, expanding any environment variables.
# Globals:
#   CI_CONFIG_PATH - the GitLab CI/CD configuration file (defaults to ".gitlab-ci.yml")
# Arguments:
#   $1 - Job name containing the variables
#######################################
ci_spec::export_all_job_variables() {
  local __yaml_global_variables
  # Note that job-level variables do not have a "description" (see https://docs.gitlab.com/ee/ci/yaml/#variablesdescription).
  __yaml_global_variables=$(yq eval ".$1.variables | keys | .[]" -- "${CI_CONFIG_PATH:-.gitlab-ci.yml}")
  
  for __var in $__yaml_global_variables; do
    ci_spec::export_job_variable "$1" "$__var"
  done
}

#######################################
# Exports the variable value from the global variables section of the GitLab CI/CD configuration file, expanding any environment variables.
# Globals:
#   CI_CONFIG_PATH - the GitLab CI/CD configuration file (defaults to ".gitlab-ci.yml")
# Arguments:
#   $1 - Global variable to export
#######################################
ci_spec::export_global_variable() {
  ci_spec::export_variable "variables" "$1"
}

#######################################
# Exports the variable value from a job section of the GitLab CI/CD configuration file, expanding any environment variables.
# Globals:
#   CI_CONFIG_PATH - the GitLab CI/CD configuration file (defaults to ".gitlab-ci.yml")
# Arguments:
#   $1 - Job name containing the variable
#   $2 - Global variable to export
#######################################
ci_spec::export_job_variable() {
  ci_spec::export_variable "$1" "variables" "$2"
}

#######################################
# Exports a single variable's value from the GitLab CI/CD configuration file, expanding any environment variables.
# Globals:
#   CI_CONFIG_PATH - the GitLab CI/CD configuration file (defaults to ".gitlab-ci.yml")
# Arguments:
#   $1..n - Path elements for yq (the last value is used as the variable name)
#######################################
ci_spec::export_variable() {
  # Note: local variables are prefixed with __ to prevent accidentally overriding exported variables for expansion. 
  # For example, if the .gitlab-ci.yml file declared FOO: "Hello $value!", it should not expand to the local variable "value" from this function.t

  # Last argument is the variable name
  local __varname
  __varname="${!#}"

  # Arguments are concatenated as ".arg1.arg2" for yq traversal operator
  local __yaml_path
  __yaml_path=$(IFS='.'; echo ".$*")

  ci_spec::export_by_path "$__varname" "$__yaml_path"
}

#######################################
# Exports the variable value from the GitLab CI/CD configuration file, expanding any environment variables.
# Globals:
#   CI_CONFIG_PATH - the GitLab CI/CD configuration file (defaults to ".gitlab-ci.yml")
# Arguments:
#   $1 - Variable name for the resulting export
#   $2 - Path for yq
#######################################
ci_spec::export_by_path() {
  # Note: local variables are prefixed with __ to prevent accidentally overriding exported variables for expansion. 
  # For example, if the .gitlab-ci.yml file declared FOO: "Hello $value!", it should not expand to the local variable "value" from this function.

  local __varname
  __varname="$1"

  local __yaml_path
  __yaml_path="$2"

  # Extract the raw value (from file $CI_CONFIG_PATH with default value of ".gitlab-ci.yml")
  local __raw_value
  __raw_value=$(yq eval "$__yaml_path" -- "${CI_CONFIG_PATH:-.gitlab-ci.yml}")

  local __value
  __value=$(ci_spec::expand_variables "$__raw_value")

  # Export the variable with given name
  export "$__varname=$__value"
}


#######################################
# Executes the script clause of the referenced job of the GitLab CI/CD configuration file.
# Globals:
#   CI_CONFIG_PATH - the GitLab CI/CD configuration file (defaults to ".gitlab-ci.yml")
# Arguments:
#   $1..n - Path element(s) for yq
#######################################
ci_spec::execute_job() {
  # Extract and run the script code (from file $CI_CONFIG_PATH with default value of ".gitlab-ci.yml")
  ci_spec::execute_scriptlet "$@" "script" "[]"
}

#######################################
# Executes the scriptlet denoted by the path elements from the GitLab CI/CD configuration file.
# A scriptlet is consists of one or more entries of a script clause (or of a before_script or after_script clause). 
# Globals:
#   CI_CONFIG_PATH - the GitLab CI/CD configuration file (defaults to ".gitlab-ci.yml")
# Arguments:
#   $1..n - Path elements for yq
#######################################
ci_spec::execute_scriptlet() {
  # Arguments are concatenated as ".arg1.arg2" for yq traversal operator
  local __yaml_path
  __yaml_path=$(IFS='.'; echo ".$*")
  
  # Extract and run the script code (from file $CI_CONFIG_PATH with default value of ".gitlab-ci.yml")
  ci_spec::execute_by_path "$__yaml_path"
}

#######################################
# Executes the referenced clause from the GitLab CI/CD configuration file.
# Globals:
#   CI_CONFIG_PATH - the GitLab CI/CD configuration file (defaults to ".gitlab-ci.yml")
# Arguments:
#   $1 - Path for yq
#######################################
ci_spec::execute_by_path() {
  # Extract the script code (from file $CI_CONFIG_PATH with default value of ".gitlab-ci.yml")
  local __scriptlet_code
  __scriptlet_code=$(yq eval "$1" -- "${CI_CONFIG_PATH:-.gitlab-ci.yml}")
  
  # Run the scriptlet code
  # Using bash -e (instead of eval) in order to exit the shell immediately on non-zero exit code
  bash -e -c "$__scriptlet_code"
}


#######################################
# Expands any environment variables in the input argument, mimicking Gitlab internal variable expansion mechanisms.
# See https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html#gitlab-internal-variable-expansion-mechanism
#
# Arguments:
#   $1 - raw value to use for expansion
# Outputs:
#   Writes the result to stdout
#######################################
ci_spec::expand_variables() {
  # Note: local variables are prefixed with __ to prevent accidentally overriding exported variables for expansion. 
  # For example, if the .gitlab-ci.yml file declared FOO: "Hello $value!", it should not expand to the local variable "value" from this function.

  local __value
  __value="$1"

  # Expanding variables using regex matching, based on https://stackoverflow.com/a/22261454/107013
  # Note: Dollar sign $ can be quoted with another Dollar sign as $$, see https://docs.gitlab.com/ee/ci/variables/#use-the--character-in-cicd-variables
  
  # Building blocks for regex:
  #
  # Start of the string: empty string or a string that does not end with a dollar sign.
  # This is a workaround because bash 5.0 regex does not support lookahead/lookbehind.
  local __regex_prefix_no_dollar
  __regex_prefix_no_dollar='^(()|(.*?[^$]))'

  # Start of the string (without check for dollar sign). 
  # Same structure as above in order to have the same amount of groups.
  local __pre_regex_percent
  __pre_regex_percent='^(()|(.*?))'

  # Variable name alphanumeric and "_".
  local __varname_regex
  __varname_regex='(\w+)'

  # Remainder of the string.
  local __regex_suffix
  __regex_suffix='(.*?)$'

  # Actual regex matchers:
  #
  # Matcher for *$var_1*
  local __regex_simple_expansion
  __regex_simple_expansion="${__regex_prefix_no_dollar}\\\$${__varname_regex}${__regex_suffix}"

  # Matcher for *${var_1}*
  local __regex_curly_expansion
  __regex_curly_expansion="${__regex_prefix_no_dollar}\\\$\\{${__varname_regex}\\}${__regex_suffix}"

  # Matcher for *%var_1%*
  local __regex_percent_expansion
  __regex_percent_expansion="${__pre_regex_percent}%${__varname_regex}%${__regex_suffix}"

  for __re in  "$__regex_simple_expansion" "$__regex_curly_expansion" "$__regex_percent_expansion"; do
    while [[ $__value =~ $__re ]]; do
      local __expansion_varname
      __expansion_varname="${BASH_REMATCH[4]}"
      # Prevent endless loop (e.g., when variable "foo" is expanded to '$foo') by duplicating $ signs
      local __escaped_expansion_value
      __escaped_expansion_value="${!__expansion_varname//\$/\$\$}"
      __value=${BASH_REMATCH[1]}${__escaped_expansion_value}${BASH_REMATCH[5]}
    done
  done

  # Print the result (remove duplicate $ signs)
  echo "${__value//\$\$/\$}"
}
