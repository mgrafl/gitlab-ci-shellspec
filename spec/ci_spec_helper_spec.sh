#shellcheck shell=bash disable=SC2317  # Don't warn about unreachable commands in this file
#
# Tests of the ci_spec_helper.sh functions

Describe 'ci_spec_helper.sh'
  Describe 'ci_spec::export_all_global_variables'
    It 'exports all global variables'
      yq() {
        capture_invocation  yq "$@"
        echo "FOO"
        echo "BAR"
      }
      ci_spec::export_global_variable() {
        export "$1=value"
      }
      When call  ci_spec::export_all_global_variables
      The count of mocks should equal 1  # yq is only called once because ci_spec::export_global_variable is also mocked
      The mock 1 should have received arguments  yq 'eval' '.variables | with_entries( select( .value | has("description") | not ) ) | keys | .[]' '--' '.gitlab-ci.yml'
      The variable FOO should equal 'value'
      The variable BAR should equal 'value'
    End
  End

  Describe 'ci_spec::export_all_job_variables'
    It 'exports all job variables'
      yq() {
        capture_invocation  yq "$@"
        echo "FOO"
        echo "BAR"
      }
      ci_spec::export_job_variable() {
        export "$2=value"
      }
      When call  ci_spec::export_all_job_variables "job-1"
      The count of mocks should equal 1
      The mock 1 should have received arguments  yq 'eval' '.job-1.variables | keys | .[]' '--' '.gitlab-ci.yml'
      The variable FOO should equal 'value'
      The variable BAR should equal 'value'
    End
  End

  Describe 'ci_spec::export_global_variable'
    It 'exports a simple variable'
      yq() {
        capture_invocation  yq "$@"
        echo "bar"
      }
      When call ci_spec::export_global_variable FOO
      The count of mocks should equal 1
      The mock 1 should have received arguments  yq 'eval' '.variables.FOO' '--' '.gitlab-ci.yml'
      The variable FOO should equal "bar"
    End
  End

  Describe 'ci_spec::export_job_variable'
    It 'exports a simple variable with expansion'
      BeforeCall "export bla=42"
      yq() {
        capture_invocation  yq "$@"
        export yq_invocation
        # Because yq is called in a subshell, variables must be preserved explicitly
        %preserve yq_invocation
        # Using single quotes to prevent variable expansion here
        # shellcheck disable=SC2016
        echo 'bar $bla'
      }
      # "Suppose that .job-1 is a base template for other jobs
      When call ci_spec::export_job_variable '".job-1"' FOO
      The count of mocks should equal 1
      The mock 1 should have received arguments  yq 'eval' '.".job-1".variables.FOO' '--' '.gitlab-ci.yml'
      The variable FOO should equal "bar 42"
    End
  End

  Describe 'ci_spec::expand_variables'
    It 'expands variables'
      BeforeCall "export bla=42 bla_13=36"
      # Using single quotes to prevent variable expansion here
      # shellcheck disable=SC2016
      When call ci_spec::expand_variables 'simple: $bla underscore: $bla_13, not matched inside word: $blafoo; curly: ${bla}foo, percent: %bla_13%, escaped: $$bla, backslash: \$bla'
      # Using single quotes to prevent variable expansion in verification
      # shellcheck disable=SC2016
      The stdout should equal 'simple: 42 underscore: 36, not matched inside word: ; curly: 42foo, percent: 36, escaped: $bla, backslash: \42'
    End

    It 'reduces double-dollar escape mechanism'
      # Using single quotes to prevent variable expansion here
      # shellcheck disable=SC2016
      When call ci_spec::expand_variables '$$begin $$$thrice ${$}ignored $$'
      # Using single quotes to prevent variable expansion in verification
      # shellcheck disable=SC2016
      The stdout should equal '$begin $$thrice ${$}ignored $'
    End

    It 'runs expansion recusively'
      BeforeCall "export bla=bl"
      # Using single quotes to prevent variable expansion here
      # shellcheck disable=SC2016
      When call ci_spec::expand_variables '${${bla}a}'
      # First expansion replaces inner ${bla} with bl, resulting in ${bla}
      The stdout should equal 'bl'
    End

    It 'prevents endless self-references'
        # Using single quotes to prevent variable expansion here
        # shellcheck disable=SC2016
      BeforeCall 'export bla=\$bla'
      # Using single quotes to prevent variable expansion here
      # shellcheck disable=SC2016
      When call ci_spec::expand_variables '$bla'
      # Using single quotes to prevent variable expansion in verification
      # shellcheck disable=SC2016
      The stdout should equal '$bla'
    End

    It 'handles special characters'
      When call ci_spec::expand_variables "a\"b'c\\"
      The stdout should equal "a\"b'c\\"
    End
  End

  Describe 'ci_spec::execute_scriptlet'
    It 'says hello'
      yq() {
        capture_invocation  yq "$@"
        echo 'echo "Hello, World!"'
      }
      When call ci_spec::execute_scriptlet  job-1 script "[0]"
      The count of mocks should equal 1
      The mock 1 should have received arguments  yq 'eval' '.job-1.script.[0]' '--' '.gitlab-ci.yml'
      The stdout should equal 'Hello, World!'
    End

    It 'exits immediately on non-zero exit code'
      yq() {
        echo '(false) || (echo "Should stop here"; exit 1)'
        echo 'echo "Should not reach this"'
      }
      When call ci_spec::execute_scriptlet  job-1 script "[0]"
      The stdout should equal 'Should stop here'
      The lines of stdout should equal 1
      The status should be failure
    End
  End

  Describe 'ci_spec::execute_job'
    It 'says hello to the world and to me'
      BeforeCall "export USER=Michael"
      yq() {
        echo 'echo "Hello, World!"'
        # Using single quotes to prevent variable expansion in echo here
        # shellcheck disable=SC2016
        echo 'echo "Hello, $USER!"'
      }
      When call ci_spec::execute_job  job-1
      The lines of stdout should equal 2
      The first line should equal 'Hello, World!'
      The second line should equal 'Hello, Michael!'
    End
  End
End
